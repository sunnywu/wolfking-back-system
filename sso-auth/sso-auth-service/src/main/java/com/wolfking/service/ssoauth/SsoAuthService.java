package com.wolfking.service.ssoauth;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.wolfking.back.core.swagger.SwaggerConfigurer;

@Controller
@EnableCaching
@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = { "com.wolfking" })
@EnableFeignClients(basePackages = "com.wolfking")
@ServletComponentScan(basePackages = { "com.wolfking" })
public class SsoAuthService implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication.run(SsoAuthService.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		SwaggerConfigurer.initSwagger("wolfking-sso-auth-service", "单点登录和权限的服务");
	}

	@RequestMapping("/")
	public String redirectSwagge() {
		String fullUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		if (fullUrl.endsWith("/"))
			fullUrl = fullUrl.substring(0, fullUrl.length() - 1);
		return "redirect:" + fullUrl + "/swagger/index.html";
	}
}
