package com.wolfking.service.ssoauth.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.wolfking.back.core.bean.Office;
import com.wolfking.back.core.mybatis.BaseMapper;
/**
 * 机构的mapper
 * <P>
 * @author   wolfking@赵伟伟
 * @mail     zww199009@163.com
 * @创作日期 2017年4月27日下午12:44:23
 * @版权     归wolfking所有
 */
@Mapper
public interface OfficeMapper extends BaseMapper<Office> {

}
