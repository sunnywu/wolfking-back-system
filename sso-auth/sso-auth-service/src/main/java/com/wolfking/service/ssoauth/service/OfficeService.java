/**
 * 
 */
package com.wolfking.service.ssoauth.service;

import org.springframework.stereotype.Service;

import com.wolfking.back.core.bean.Office;
import com.wolfking.back.core.mybatis.BaseService;
import com.wolfking.service.ssoauth.mapper.OfficeMapper;

/**
 * 机构的service
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月27日下午12:51:38
 * @版权 归wolfking所有
 */
@Service
public class OfficeService extends BaseService<OfficeMapper, Office> {

	
}
