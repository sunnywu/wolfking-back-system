package com.wolfking.service.ssoauth.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.mybatis.BaseMapper;

/**
 * 用户的mapper映射
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月23日 上午10:56:28
 * @copyright wolfking
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
	@Select("select u.* from sys_user u " + "join sys_user_role ur on u.id = ur.user_id "
			+ "where ur.role_id = #{roleId}")
	List<User> getUserByRoleId(@Param("roleId") String roleId);

	@Select("select DISTINCT (m.permission) from sys_user_role ur "
			+ "join sys_role_menu rm  on ur.role_id = rm.role_id left join sys_menu m on rm.menu_id = m.id  "
			+ "where ur.user_id = #{userId} and length(m.permission) >0 ")
	List<String> getUserAllAuthCodes(@Param("userId") String userId);

	@Select("select DISTINCT m.id from  sys_menu m JOIN sys_role_menu rm  on m.id = rm.menu_id JOIN sys_user_role ur on ur.user_id = rm.role_id WHERE ur.user_id = #{userId} and m.is_show = '1'")
	List<String> getUserAllMenuIds(@Param("userId") String userId);
}
