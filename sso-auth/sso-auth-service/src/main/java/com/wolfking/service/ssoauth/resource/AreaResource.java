package com.wolfking.service.ssoauth.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.Area;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.util.ResponseUtil;
import com.wolfking.service.ssoauth.service.AreaService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Component
@Path("/area")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/area", description = "区域的服务", produces = MediaType.APPLICATION_JSON)
public class AreaResource {

	@Autowired
	private AreaService areaService;

	@POST
	@ApiOperation(value = "区域添加接口", httpMethod = "POST", notes = "区域添加接口", response = Boolean.class)
	public Response addArea(@RequestBody Area area) {
		return ResponseUtil.okResponse(areaService.add(area));
	}

	@PUT
	@ApiOperation(value = "区域修改接口", httpMethod = "PUT", notes = "区域修改接口", response = Boolean.class)
	public Response updateArea(@RequestBody Area area) {
		return ResponseUtil.okResponse(areaService.update(area));
	}

	@GET
	@ApiOperation(value = "查询所有的区域", httpMethod = "GET", notes = "查询所有的区域", response = Area.class, responseContainer = "List")
	public Response getAllArea() {
		return ResponseUtil.okResponse(areaService.findAll());
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "根据ID查询区域接口", httpMethod = "GET", notes = "根据ID查询区域接口", response = Area.class)
	public Response getArea(@PathParam("id") @ApiParam(required = true, name = "id", value = "区域ID") String id) {
		Area area = areaService.getById(id);
		return ResponseUtil.okResponse(area);
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "根据ID删除区域接口", httpMethod = "DELETE", notes = "根据ID删除区域接口", response = Boolean.class)
	public Response deleteArea(@PathParam("id") @ApiParam(required = true, name = "id", value = "区域ID") String id) {
		return ResponseUtil.okResponse(areaService.deleteById(id));
	}

	@POST
	@Path("/page")
	@ApiOperation(value = "分页查询区域信息", httpMethod = "POST", notes = "分页查询区域信息", response = Area.class, responseContainer = "PageInfo")
	public Response pageArea(@RequestBody Area area,
			@HeaderParam("pageNum") @ApiParam(name = "pageNum", value = "页码", defaultValue = "1") int pageNum,
			@HeaderParam("pageSize") @ApiParam(name = "pageSize", value = "每页大小", defaultValue = "10") int pageSize) {
		PageInfo<Area> pageInfo = areaService.pageVague(area, pageNum, pageSize);
		return ResponseUtil.okResponse(pageInfo);
	}
}
