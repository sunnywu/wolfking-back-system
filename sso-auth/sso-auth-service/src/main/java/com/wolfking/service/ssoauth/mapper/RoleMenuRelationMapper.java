package com.wolfking.service.ssoauth.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.wolfking.back.core.mybatis.BaseMapper;
import com.wolfking.service.ssoauth.relation.RoleMenuRelation;

/**
 * 角色和菜单的关联关系
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月13日 下午9:31:03
 * @copyright wolfking
 */
@Mapper
public interface RoleMenuRelationMapper extends BaseMapper<RoleMenuRelation>{

}
