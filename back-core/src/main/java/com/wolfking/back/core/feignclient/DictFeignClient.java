package com.wolfking.back.core.feignclient;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.Dict;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.feign.JerseyFeignConfig;

/**
 * 字典服务的远程调用的client
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月25日下午3:44:31
 * @版权 归wolfking所有
 */
@FeignClient(name = "sso-auth-service", configuration = { JerseyFeignConfig.class })
public interface DictFeignClient {

	/**
	 * 分页查询字典
	 * 
	 * @param dict
	 * @param arg1
	 * @param arg2
	 * @return
	 */
	@POST
	@Path("/wolfking/dict/page")
	ResponseEntity<PageInfo<Dict>> pageDict(@RequestBody(required = true) Dict dict,
			@HeaderParam("pageNum") int pageNum, @HeaderParam("pageSize") int pageSize);

	/**
	 * 根据ID获取字典
	 * 
	 * @param id
	 * @return
	 */
	@GET
	@Path("/wolfking/dict/{id}")
	ResponseEntity<Dict> getDict(@PathParam("id") String id);

	/**
	 * 根据ID删除字典
	 * 
	 * @param id
	 * @return
	 */
	@DELETE
	@Path("/wolfking/dict/{id}")
	ResponseEntity<Boolean> deleteDict(@PathParam("id") String id);

	/**
	 * 添加字典
	 * 
	 * @param dict
	 * @return
	 */
	@POST
	@Path("/wolfking/dict")
	ResponseEntity<Boolean> addDict(@RequestBody(required = true) Dict dict);

	/**
	 * 修改字典
	 * 
	 * @param dict
	 * @return
	 */
	@PUT
	@Path("/wolfking/dict")
	ResponseEntity<Boolean> updateDict(@RequestBody(required = true) Dict dict);

	/**
	 * 根据类别获取字典的list
	 * 
	 * @param arg0
	 * @return
	 */
	@GET
	@Path("/wolfking/dict/getByType")
	ResponseEntity<List<Dict>> getByType(@QueryParam("type") String type);

	/**
	 * 获取所有的字典类别
	 * 
	 * @return
	 */
	@GET
	@Path("/wolfking/dict/getAllType")
	ResponseEntity<List<String>> getAllType();
}
