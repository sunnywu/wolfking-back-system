/**
 * 
 */
package com.wolfking.back.core.feignclient;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.Office;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.feign.JerseyFeignConfig;

/**
 * 机构的远程调用feign
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月4日下午3:37:03
 * @版权 归wolfking所有
 */
@FeignClient(name = "sso-auth-service", configuration = { JerseyFeignConfig.class })
public interface OfficeFeignClient {
	@PUT
	@Path("/wolfking/office")
	ResponseEntity<Boolean> updateOffice(@RequestBody(required = true) Office office);

	@POST
	@Path("/wolfking/office")
	ResponseEntity<Boolean> addOffice(@RequestBody(required = true) Office office);

	@GET
	@Path("/wolfking/office")
	ResponseEntity<List<Office>> getAllOffice();

	@GET
	@Path("/wolfking/office/{id}")
	ResponseEntity<Office> getOffice(@PathParam("id") String id);

	@DELETE
	@Path("/wolfking/office/{id}")
	ResponseEntity<Boolean> deleteOffice(@PathParam("id") String id);

	@POST
	@Path("/wolfking/office/page")
	ResponseEntity<PageInfo<Office>> pageOffice(@RequestBody(required = true) Office office,
			@HeaderParam("pageNum") int pageNum, @HeaderParam("pageSize") int pageSize);

	@GET
	@Path("/wolfking/office/findAllChild/{id}")
	ResponseEntity<List<Office>> getAllChildOffice(@PathParam("id") String id);
}
