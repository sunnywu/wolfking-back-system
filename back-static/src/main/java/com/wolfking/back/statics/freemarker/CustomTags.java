package com.wolfking.back.statics.freemarker;

import org.springframework.stereotype.Component;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.SimpleHash;

/**
 * freemarker的自定义标签
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月28日下午3:50:45
 * @版权 归wolfking所有
 */
@Component
public class CustomTags extends SimpleHash {
	private static final long serialVersionUID = -2221904472687006908L;

	public CustomTags() {
		super(new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25).build());
		put("cookies", new CookieTag());
		put("hasPermission", new HasPermissionTag());
		put("treeView", new TreeViewTag());
	}
}
